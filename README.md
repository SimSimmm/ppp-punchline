
# Project Description
Punchline.com is a website created to allow users to write and post punchlines and read other users punchlines.
Anyone can read any punchline posted on the website so there are some rules visible in the settlement part.
Each written punchline has a score that can increase when a user clicks on it, users can also comment the punchlines.

# How to use
- install Mysql
- create a database
    - database name : punchbox
    - database username : punchy
    - database user code : punchcode
- launch the mysql server
- build the project and run the application
- go to your web browser to localhost on port 8080 (127.0.0.1:8080)

# Other informations
Features implemented
- post punchline
- upvote punchline
- see all the punchlines from a user
- show a random punchline
- show the top punchliner
- show the 3 top punchlines
- sort messages by date
- sort punchlines by score

Features we wanted to implement
- real user session and connexion
- a tag system to sort punchlines
- a system to comment punchlines
- a system of achievments
- post gif and png
- bookmark prefered punchlines
