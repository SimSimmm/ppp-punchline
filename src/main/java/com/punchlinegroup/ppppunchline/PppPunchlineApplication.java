package com.punchlinegroup.ppppunchline;

import java.util.Date;
import com.punchlinegroup.ppppunchline.models.Message;
import com.punchlinegroup.ppppunchline.models.User;
import com.punchlinegroup.ppppunchline.persistence.UserDao;
import com.punchlinegroup.ppppunchline.persistence.MessageDao;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;

@SpringBootApplication
@EnableJpaRepositories
public class PppPunchlineApplication {

    @Autowired
    public UserDao userDao;

    @Autowired
    public MessageDao messageDao;

    public static void main(String[] args) {

        SpringApplication.run(PppPunchlineApplication.class, args);
    }

    @PostConstruct
    public void init() {

        // Remove old entries in the database
        userDao.deleteAll();
        messageDao.deleteAll();

        //Add 2 users localy in the database for testing
        userDao.save(new User(null, "simon", "test"));
        userDao.save(new User(null, "roxi", "test2"));

        // Add some messages localy in the database for testing
        messageDao.save(new Message( null,"Yo moma's is so fat when she got on the scale it said, \"I need your weight not your phone number.\"", "roxxxxane", 10000, new Date()));
        messageDao.save(new Message( null,"Yo mama's classic joke", "simon", 9000, new Date()));
        messageDao.save(new Message( null,"Yo moma's is so fat that Dora can't even explore her!", "swag master", 13000,new Date()));
        messageDao.save(new Message( null,"Yo mama's is so fat she doesn't need the internet, because she's already world wide.", "Socrate", 13000,new Date()));
        messageDao.save(new Message( null,"Windows Vista supports real multitasking - it can boot and crash simultaneously", "Aristote", 9000, new Date()));
        messageDao.save(new Message( null,"insert funny joke here", "swag master", 8000,new Date()));
        messageDao.save(new Message( null,"--> Buy better grades here <--", "Shakespeare", 42,new Date()));
        messageDao.save(new Message( null,"Yo mama's so stupid, she brought a spoon to the super bowl", "swag master", 13000,new Date()));
        messageDao.save(new Message( null,"In a world without fences and walls, who needs Gates and Windows?", "simon", 9000, new Date()));
        messageDao.save(new Message( null,"Yo mama's so fat her blood type is Nutella", "roxxxxane", 7000,new Date()));
        messageDao.save(new Message( null,"Yo mama's so old that she walked into an antique store and they kept her", "cuteOtter", 2000,null));
        messageDao.save(new Message( null,"Yo mama's so fat that she went to the movie theatre and sat next to everyone", "Ironic Meteor", 13000,new Date()));
        messageDao.save(new Message( null,"Mac users swear by their Mac, PC users swear at their PC", "Jules Verne", 6000, new Date()));
        messageDao.save(new Message( null,"My attitude isn't bad. It's in beta. ", "simon", 9000, new Date()));
        messageDao.save(new Message( null,"Yo mama's so fat she goes to KFC and licks other people’s fingers", "Booba", 9590,new Date()));
        messageDao.save(new Message( null,"Yo mama's so stupid when your dad sad it was chilly outside, she ran out the door with a spoon!", "Huba booba", 7000,new Date()));
        messageDao.save(new Message( null,"Yo mama's so fat, I took a picture of her last Christmas and it’s still printing", "Marsupilami", 13000,new Date()));
        messageDao.save(new Message( null,"Programming is like sex, one mistake and you have to support it for the rest of your life", "Marie Antoinette", 9000, new Date()));
        messageDao.save(new Message( null,"Yo mama's so dumb she tried to climb mountain dew", "Emperor of punk", 4000,new Date()));
        messageDao.save(new Message( null,"I would love to change the world, but they won't give me the source code.", "cuteOtter", 50000, new Date()));
        messageDao.save(new Message( null,"Unix is user friendly. It's just selective about who its friends are.", "BunButtCorgi", 20000, new Date()));
        messageDao.save(new Message( null,"CAPS LOCK – Preventing Login Since 1980. ", "simon", 50, new Date()));
        messageDao.save(new Message( null,"Yo mama's so old her birth certificate says “expired”", "swag master", 200,new Date()));
        messageDao.save(new Message( null,"Yo mama's so dumb she went to the dentist to get her Bluetooth fixed", "Victor Hugo", 14000,new Date()));
        messageDao.save(new Message( null,"What was Captain Hook’s name when he had two hands?", "Anne Franck", 5000,new Date()));
        messageDao.save(new Message( null,"Q: What happens when the doctor goes back in time and sees himself ? \n" + "A: Its a pair-a-docs! ", "Naruto", 8000,new Date()));
        messageDao.save(new Message( null,"There are 10 types of people in the world: those who understand binary, and those who don't", "Balzac", 30000,new Date()));
        messageDao.save(new Message( null,"Windows Vista supports real multitasking - it can boot and crash simultaneously", "Conan Doyle", 900,new Date()));
        messageDao.save(new Message( null,"Chuck Norris doesn't call the wrong number. You answered the wrong phone. ", "Agatha Christie", 2000000000,new Date()));

    }

}
