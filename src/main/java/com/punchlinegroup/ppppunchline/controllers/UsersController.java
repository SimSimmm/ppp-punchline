package com.punchlinegroup.ppppunchline.controllers;

import com.punchlinegroup.ppppunchline.models.User;
import com.punchlinegroup.ppppunchline.persistence.UserDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UsersController {

    private final UserDao userDao;

    public UsersController(UserDao userDao) {
        this.userDao = userDao;
    }

    @GetMapping("/user")
    public String addUserForm(Model model) {
        //model.addAttribute("user", new User());
        return "add_member";
    }

    @PostMapping("/user")
    public void addUser(User user, Model model) {
        userDao.save(user);
        //return "redirect:/users";
    }

    /*
When "validateConnexion" is used in a button/form/... Page is redirected to "users"
 */
    @GetMapping("/index")
    public String index (Model model) {
        return "";
    }

    @PostMapping("/index")
    public String index(User user, Model model) {
        return "redirect:/";
    }

    @GetMapping("/profil")
    public String profil (Model model) {
        return "profil";
    }

    @PostMapping("/profil")
    public String profil(User user, Model model) {
        return "redirect:/profil";
    }

    @GetMapping("/connection")
    public String connexionForm(Model model) {
        return "connection";
    }

    @PostMapping("/toConnect")
    public String toConnect(User user, Model model) {
        return "redirect:/connection";
    }

    @GetMapping("/inscription")
    public String inscription(Model model) {
        return "inscription";
    }

    @PostMapping("/inscription")
    public String inscription(User user, Model model) {
        return "redirect:/inscription";
    }

    @PostMapping("/toHome")
    public String toHome(User user, Model model) {
        return "redirect:/home";
    }

    @GetMapping("/rules")
    public String rules(Model model) {
        return "rules";
    }

    @PostMapping("/rules")
    public String rules(User user, Model model) {
        return "redirect:/rules";
    }

}
