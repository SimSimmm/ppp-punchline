package com.punchlinegroup.ppppunchline.controllers;

import com.punchlinegroup.ppppunchline.models.Message;
import com.punchlinegroup.ppppunchline.persistence.MessageDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;


@Controller
public class MessagesController {

    private final MessageDao messageDao;

    public MessagesController(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public void getRandomMessage(Model model){
        int var = (int) messageDao.count();
        int randomNum = ThreadLocalRandom.current().nextInt(1, var + 1);
        model.addAttribute("randomMessage", messageDao.findById(randomNum).get().getMsg());
    }

    public void getBestPunchliners(Model model){
        model.addAttribute("bestPunchlines", messageDao.findAllByOrderByScoreDesc());
    }

    @GetMapping("/")
    public String redirectToIndex(Model model){
        model.addAttribute("data", messageDao.findAllByOrderByDateDesc());
        model.addAttribute("nmsg", new Message());

        getRandomMessage(model);
        getBestPunchliners(model);

        return "index";
    }

    // "/home" is displayed when a user is connected instead of "/"
    @GetMapping("/home")
    public String redirectToHome(Model model){
        model.addAttribute("data", messageDao.findAllByOrderByDateDesc());
        model.addAttribute("nmsg", new Message());

        getRandomMessage(model);
        getBestPunchliners(model);

        return "home";
    }

    @PostMapping("/message")
    public String addMessage(Message message) {

        Date date = new Date();

        if (message.getMsg() == ""){
            return "redirect:/home";
        }else{
            message.setUser("Anonym");
            message.setScore(0);
            message.setDate(date);
            messageDao.save(message);
            return "redirect:/home";
        }
    }

    @GetMapping("/upvote/{id}")
    public String upvoteMessage(@PathVariable Integer id) {
        Message msg = messageDao.findById(id).get();
        msg.setScore(msg.getScore() + 1);
        messageDao.save(msg);
        return "redirect:/home";
    }

    // Find all the messages of a specific user
    @GetMapping("/{username}")
    public String showUserMessage(@PathVariable String username, Model model) {
        model.addAttribute("msg_list", messageDao.findAllByUser(username));
        return "user_posts";
    }

}
