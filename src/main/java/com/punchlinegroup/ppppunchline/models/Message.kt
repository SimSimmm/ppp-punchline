package com.punchlinegroup.ppppunchline.models

import java.util.*
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
class Message (@Id @GeneratedValue var id: Int? = null,
               var msg: String? = null,
               var user: String ? = null,
               var score:Int ? = null,
               var date: Date ? = null
               ){


}