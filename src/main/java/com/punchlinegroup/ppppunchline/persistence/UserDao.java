package com.punchlinegroup.ppppunchline.persistence;

import com.punchlinegroup.ppppunchline.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<User, Integer> {
}
