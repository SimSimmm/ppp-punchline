package com.punchlinegroup.ppppunchline.persistence;

import com.punchlinegroup.ppppunchline.models.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MessageDao extends CrudRepository<Message, Integer> {

    List<Message> findAllByUser(String user);
    List<Message> findAllByOrderByScoreDesc();
    List<Message> findAllByOrderByDateDesc();
}
